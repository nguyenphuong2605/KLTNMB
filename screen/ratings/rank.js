/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { Component, useEffect, useMemo, useState } from 'react';
import { FlatList, Image, StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { connect } from 'react-redux';
import DropDownPicker from 'react-native-dropdown-picker';

const Rank = (props) => {
  const [idCurrentMember, setIdCurrentMember] = useState('');
  const [open, setOpen] = useState(false);
  const [valueRD, setValueRD] = useState(props.listTopic[0]._id);

  useEffect(() => {
    AsyncStorage.getItem('idMember').then((value) => {
      if (value) {
        setIdCurrentMember(value);
      }
    });
  }, []);

  useEffect(() => {
    if (props.idmember) {
      AsyncStorage.getItem('idMember').then((value) => {
        if (value) {
          setIdCurrentMember(props.idmember);
        }
      });
    }
  }, [props.idmember]);

  const star = (list) => {
    var resuilt = 0;
    for (let i = 0; i < list.length; i++) {
      if (list[i].idTopic === valueRD) {
        resuilt += Number(list[i].star);
      }
    }
    return resuilt.toFixed(1);
  };

  const sort = (id) => {
    const { allMember } = props;
    if (allMember) {
      return allMember.sort((a,b) => a.idTopic === id && b.idTopic === id && star(b.resuilt) -  star(a.resuilt));
    }
    return [];
  };

  useEffect(() => {
    if (valueRD) {
      sort(valueRD);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [valueRD]);

  const myRank = () => {
    const { allMember } = props;
    for (var i = 0; i < allMember.length; i++) {
      if (allMember[i]._id === props.idmember) {
        return (
          <View style={styles.contentBottom}>
            <View style={styles.index}>
              <Text>{i + 1}</Text>
            </View>
            <View style={styles.profile}>
              <Image
                style={styles.image}
                source={
                  allMember[i].avatar || allMember[i].image
                    ? { uri: allMember[i].avatar || allMember[i].image }
                    : require('../../assets/image/english.png')
                }
              />
              <Text style={styles.name}>
                {allMember[i].ho} {allMember[i].ten}
              </Text>
            </View>
            <View style={styles.star}>
              <Text style={styles.starNumber}>
                {star(allMember[i].resuilt)}
              </Text>
              <Icon name="star" color={'yellow'} size={25} />
            </View>
          </View>
        );
      }
    }
  };

  const items = useMemo(() => props.listTopic.map(item => ({
    label: item.name,
    value: item._id,
  })), [props.listTopic]);


  console.log(valueRD, props.listTopic);

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.container}>
        <View style={{
          paddingHorizontal: 10,
          marginTop: 16,
          marginBottom: 16,
        }}>
          <DropDownPicker
            open={open}
            value={valueRD}
            items={items}
            setOpen={setOpen}
            setValue={setValueRD}
            showTickIcon={false}
            closeOnBackPressed
            style={{
              elevation: 5,
              borderWidth: 0,
              padding: 0,
              margin: 0,
              minHeight: 50,
            }}
            labelStyle={{
              fontSize: 13,
            }}
            disableBorderRadius
            dropDownContainerStyle={{
              borderWidth: 0,
            }}
          />
        </View>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={sort()}
          renderItem={({ item, index }) => {
            return (
              <View style={styles.content}>
                <View style={styles.index}>
                  <Text> {index + 1} </Text>
                </View>
                <View style={styles.profile}>
                  <View style={styles.wrapperImage}>
                    <Image
                      style={styles.image}
                      source={
                        item.avatar || item.image
                          ? { uri: item.avatar || item.image }
                          : require('../../assets/image/english.png')
                      }
                    />
                    {index + 1 === 1 ? (
                      <Icon
                        style={styles.crown}
                        name="crown"
                        size={20}
                        color="yellow"
                      />
                    ) : index + 1 === 2 ? (
                      <Icon
                        style={styles.crown}
                        name="crown"
                        size={20}
                        color="gray"
                      />
                    ) : index + 1 === 3 ? (
                      <Icon
                        style={styles.crown}
                        name="crown"
                        size={20}
                        color="#9e7e20"
                      />
                    ) : null}
                  </View>
                  <Text style={styles.name}>
                    {item.ho} {item.ten}
                  </Text>
                </View>
                <View style={styles.star}>
                  <Text style={styles.starNumber}>
                    {star(item.resuilt)}{' '}
                  </Text>
                  <Icon name="star" color={'yellow'} size={25} />
                </View>
              </View>
            );
          }}
          keyExtractor={(item) => `${item._id}`}
        />
      </View>
      {myRank()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 10,
    backgroundColor: '#FFF',
    borderRadius: 20,
    alignItems: 'stretch',
    flex: 1,
    overflow: 'hidden',
  },
  content: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    backgroundColor: 'white',
  },
  contentBottom: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFF',
    elevation: 10,
  },
  index: {
    backgroundColor: 'orange',
    height: 30,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  profile: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 20,
  },
  image: {
    height: 30,
    width: 30,
    borderRadius: 20,
  },
  name: {
    paddingLeft: 20,
    fontSize: 13,
  },
  starNumber: {
    fontWeight: 'bold',
    fontSize: 18,
    marginRight: 4,
  },
  star: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapperImage: {
    position: 'relative',
    zIndex: 1,
  },
  crown: {
    top: -10,
    right: -10,
    position: 'absolute',
    transform: [{ rotate: '45deg' }],
  },
});

const mapStateToProps = (state) => {
  return {
    allMember: state.rank.data,
    idmember: state.auth.id,
    listTopic: state.topic.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Rank);
